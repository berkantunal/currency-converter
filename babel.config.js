module.exports = {
  plugins: [
    [
      "babel-plugin-root-import",
      {
        paths: [
          {
            rootPathSuffix: "./src/",
            rootPathPrefix: "~/"
          }
        ]
      }
    ]
  ],
  presets: ["@vue/cli-plugin-babel/preset"]
};
