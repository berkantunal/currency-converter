const state = {
  list: []
};

const mutations = {
  setList(state, list) {
    state.list = list;
  }
};

const actions = {
  addHistory({ state, commit }, history) {
    const { list } = state;

    history.date = new Date();
    list.push(history);

    commit("setList", list);
  }
};

const getters = {
  history(state) {
    return state.list;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
