import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import history from "~/store/history";

export default new Vuex.Store({
  modules: {
    history
  }
});
