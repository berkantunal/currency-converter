import axios from "axios";

const service = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 5000
});

service.interceptors.response.use(
  ({ status, data }) => {
    if (status === 200) {
      return data;
    }
  },
  error => {
    // eslint-disable-next-line
    console.error("reqest error:", error);
    return {};
  }
);

export default service;
