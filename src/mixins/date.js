export default {
  filters: {
    dateFormat(date) {
      const dateObj = {
        month: date.getMonth() + 1,
        day: date.getDate(),
        hours: date.getHours(),
        minutes: date.getMinutes(),
        seconds: date.getSeconds(),
        year: date.getFullYear()
      };

      for (const key in dateObj) {
        if (key != "year") {
          let dateData = dateObj[key].toString();

          if (dateData.length < 2) {
            dateObj[key] = "0" + dateData;
          }
        }
      }

      return `${dateObj.day}.${dateObj.month}.${dateObj.year} ${dateObj.hours}:${dateObj.minutes}:${dateObj.seconds}`;
    }
  }
};
