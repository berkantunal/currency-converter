import request from "~/utils/request";

export const getLatestCurrencies = () => {
  return request.get("/latest").then(currencies => {
    currencies.rates["EUR"] = 1;
    return currencies;
  });
};
